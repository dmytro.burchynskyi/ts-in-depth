/* eslint-disable no-redeclare */

import { ReferenceItem, UL, RefBook, Shelf } from "./classes";
import { Book, Librarian, Logger, Magazine } from "./interfaces";
import { getAllBooks, printRefBook, purge, getObjectProperty, createCustomer, getBooksByCategory, logCategorySearch, getBooksByCategoryPromise, logSearchResults } from "./functions";
import { Library } from "./classes/library";
import { Category } from "./enums";
import { BookRequiredFields, CreateCustomerFunctionType, UpdatedBook } from "./type";
// import type { Library } from "./classes/library";

showHello('greeting', 'TypeScript');

function showHello(divName: string, name: string) {
    const elt = document.getElementById(divName);
    elt.innerText = `Hello from ${name}`;
}

// =================================================================================
// console.log(getAllBooks());
// logFirstAvailable(getAllBooks());
// ============================================================================================

// ===================================================================================================
// Task 02.01

// logBookTitles(getBookTitlesByCategory(Category.JavaScript))
// console.log(getBookAuthorByIndex(0))
// caclTotalPages();

// Task 03.01

// const myID: string = createCustomerID('Ann', 10);
// console.log(myID);

// let idGenerator: (name: string, id: number) => string;
// let idGenerator: typeof createCustomerID;
// idGenerator = (name: string, id: number) => `${id}/${name}`;
// idGenerator = createCustomerID;

// Task 03.02
// createCustomer('Anna');
// createCustomer('Anna', 26);
// createCustomer('Anna', 26, 'Dnipro');

// console.log(getBookTitlesByCategory())
// console.log(getBookTitlesByCategory(Category.CSS))
// logFirstAvailable()

// console.log(getBookByID(1))
// console.log(checkoutBooks('NoName Customer', 1, 2, 3, 4))

// Task 03.03
// console.log(getTitles(1, true))
// console.log(getTitles(true))
// console.log(getTitles(false))
// console.log(getTitles(2, false))
// console.log(getTitles('Lea Verou'))

// Task 03.04
// console.log(bookTitleTransform('Learn TypeScript'))
// console.log(bookTitleTransform(1,2,3))
// console.log(bookTitleTransform({}))

// Task 04.01
// const myBook: Book = {
//     id: 5,
//     title: 'Colors,Backgrounds, and Gradients',
//     author: 'Eric A.Meyer',
//     available: true,
//     category: Category.CSS,
//     // year: 2015,
//     // copies: 3
//     pages: 200,
//     markDamaged: (reason: string) => console.log(`Damaged: ${reason}`),
// }
// printBook(myBook)
// myBook.markDamaged('missing back cover')

// Task 04.02
// const logDamage: Logger = (reason: string) => console.log(`Damaged: ${reason}`)
// logDamage('missing back cover')

// Task 04.03
// const favoriteAuthor: Author = {
//     name: 'Anna',
//     email: 'anna@example.com',
//     numBooksPublished: 2
// }

// const favoriteLibrarian: Librarian = {
//     name: 'Boris',
//     email: 'boris@example.com',
//     department: 'Classical Literature',
//     assistCustomer: null
// }

// Task 04.04

// const offer: any = {
//     book: {
//         title: 'Essential TypeScript'
//     },
// }
// console.log(offer.magazine)
// console.log(offer.magazine?.getTitle())
// console.log(offer.book.getTitle?.())
// console.log(offer.book.authors?.[0])
// console.log(offer.book.authors?.[10]?.name)

// Task 04.05

// console.log(getProperty(myBook, 'title'))
// console.log(getProperty(myBook, 'markDamaged'))
// console.log(getProperty(myBook, 'isbn'))

// const duplicateArr = <T>(arr: T[], times: number = 2): T[] => {
//     return Array(times)
//         .fill([...arr])
//         .reduce((a, b) => a.concat(b));
// }
// console.log(duplicateArr<number>([1, 2, 3]))
// console.log(duplicateArr<string>(['BANANo'], 3))

// function logger(data: string[], serviceInfo?: { serviceName: string, serviceId: number }) {
//     if (serviceInfo == undefined) {
//         serviceInfo = {
//             serviceName: "global",
//             serviceId: 1,
//         };
//     }
//     let obj = {};

//     data.forEach(function (value, i) {
//         //console.log('%d: %s', i, value);
//         obj[serviceInfo.serviceId + "-" + i] =
//             "[" + serviceInfo.serviceName + "] " + value;
//     });
//     return obj
// }
// console.log(logger(['Fatal error', 'Data corrupted']))
// console.log(logger(['Wrong email', 'Wrong password', 'Success login'], { serviceName: 'auth_service', serviceId: 3 }))
// ---------------------------------------------------------------------------------------
// console.log(getBookByID(1));

// ------------------------------------Task 03.03---------------------

// getTitles(1, true);

// ----------------Task03.04------------------
// console.log(bookTitleTransform('Learn TypeScript'));
// ----------------------Task04.01----------------------------------------
// const myBook = {
//     id: 5,
//     title: 'Colors, Backgrounds, and Gradients',
//     author: 'Eric A. Meyer',
//     available: true,
//     category: Category.CSS,
//     year: 2015,
//     copies: 3,
// }
// printBook(myBook)

// ===================================Classes============================================

// ---------Task 0.5.01--------------------

// const ref = new ReferenceItem(1, 'Learn typescript', 2022);
// console.log(ref);
// ref.publisher = 'abc group';
// console.log(ref.publisher)
// console.log(ref.getID())

// -----------------Task 05.02, 05.03-------------------------------

// const refBook: Encyclopedia = new Encyclopedia(1, 'Learn TypeScript', 2022, 2)
// const refBook: RefBook = new RefBook(1, 'Learn TypeScript', 2022, 2)
// refBook.printItem();
// console.log(refBook)
// console.log(refBook.getID())
// refBook.printCitation()

// ----------------------------Task 05.04-------------------------
// const favoriteLibrarian: Librarian = new UL.UniversityLibrarian();
// const favoriteLibrarian: Librarian & UL.A = new UL.UniversityLibrarian();
// favoriteLibrarian.name = 'Anna'
// favoriteLibrarian.assistCustomer('Boris', 'Learn TypeScript')
// favoriteLibrarian.a = 2

// let a: number | string;
// (<number>a).toLocaleUpperCase()
// if (typeof a === 'string') {
//     a.toLocaleUpperCase()
// } else {
//     a.toFixed()
// }

// -------------------------Task 05.05------------------------------
// const personBook: PersonBook = {
//     name: 'Anna',
//     author: 'Anna',
//     available: false,
//     category: Category.Angular,
//     email: 'anna@example.com',
//     id: 1,
//     title: 'unknown'
// }

// const options: TOptions = { duration: 20 }
// const options = {}
// const options2 = setDefaultConfig(options)
// console.log(options)
// console.log(options2)
// console.log(Object.is(options, options2))

// ------------------------------------Task 06.03, 06.04------------------------------
// const refBook: RefBook = new RefBook(1, 'Learn TypeScript', 2022, 2)
// printRefBook(refBook)

// const favoriteLibrarian: Librarian = new UniversityLibrarian();
// printRefBook(favoriteLibrarian)
// const favoriteLibrarian: Librarian = new UL.UniversityLibrarian();
// printRefBook(favoriteLibrarian)

// -------------------------------------Task 06.05------------
// const flag = true;
// if (flag) {
//     import('./classes')
//         .then(o => {
//             const reader = new o.Reader();
//             reader.name = 'Anna'
//             reader.take(getAllBooks()[0])

//             console.log(reader)
//         })
//         .catch(err => console.log(err))
//         .finally(() => console.log('Completed !'))
// }
// if (flag) {
//     const o = await import('./classes')
//     const reader = new o.Reader();
//     reader.name = 'Anna'
//     reader.take(getAllBooks()[0])

//     console.log(reader)
// }

// --------------------------------------Task 06.06-----------
// let library: Library = new Library()
// let library: Library = {
//     id: 1,
//     address: '',
//     name: 'Anna'
// }

// ----------------------------Task 07.01------------------
// const inventory: Book[] = [
//     { id: 10, title: 'The C Programming Language', author: 'K & R', available: true, category: Category.Software },
//     { id: 11, title: 'Code Complete', author: 'Steve McConnell', available: true, category: Category.Software },
//     { id: 12, title: '8-Bit Graphics with Cobol', author: 'A. B.', available: true, category: Category.Software },
//     { id: 13, title: 'Cool autoexec.bat Scripts!', author: 'C. D.', available: true, category: Category.Software }
// ]

// const result = purge<Book>(inventory);
// const result1 = purge(inventory);
// console.log(result1)
// const result2 = purge([1, 2, 3, 4])
// console.log(result2)

// ------------------------Task 07.02, 07.03----------------------
// const bookShelf: Shelf<Book> = new Shelf<Book>();
// inventory.forEach(book => bookShelf.add(book))
// console.log(bookShelf.getFirst().title)

// const magazines: Magazine[] = [
//     { title: 'Programming Language Monthly', publisher: 'Code Mags' },
//     { title: 'Literary Fiction Quarterly', publisher: 'College Press' },
//     { title: 'Five Points', publisher: 'GSU' }
// ]

// const magazineShelf = new Shelf<Magazine>();
// magazines.forEach(mag => magazineShelf.add(mag))
// console.log(magazineShelf.getFirst().title)

// magazineShelf.printTitles()
// console.log(magazineShelf.find('Five Points'))

// console.log(getObjectProperty(magazines[0], 'title'))
// console.log(getObjectProperty(inventory[1], 'author'))

// -----------------------------Task 07.04-------------------
// const bookRequiredFields: BookRequiredFields = {
//     author: 'Anna',
//     available: false,
//     category: Category.Angular,
//     id: 1,
//     markDamaged: null,
//     pages: 200,
//     title: 'Learn Angular'
// }

// const updatedBook: UpdatedBook = {
//     id: 1,
//     pages: 300
// }

// let params: Parameters<CreateCustomerFunctionType>;
// params = ['Anna', 30, 'Kyiv']
// createCustomer(...params)

// ------------------------------Task 08.01,08.02----------------
// const favoriteLibrarian1 = new UL.UniversityLibrarian();
// const favoriteLibrarian2 = new UL.UniversityLibrarian();
// favoriteLibrarian1['a'] = 1;
// UL.UniversityLibrarian['a'] = 2

// console.log(favoriteLibrarian1)
// favoriteLibrarian1.name = 'Anna'
// favoriteLibrarian1['printLibrarian']()

// -----------------------------Task 08.03-----------------------------------
// const favoriteLibrarian1 = new UL.UniversityLibrarian();
// console.log(favoriteLibrarian1)
// favoriteLibrarian1.assistFaculty = null;
// favoriteLibrarian1.teachCommunity = null;

// ----------------------------------Task 08.04---------------------------------
// const refBook: RefBook = new RefBook(1, 'Learn TypeScript', 2022, 2)
// refBook.printItem()

// ------------------------------Task 08.05-------------------------------
// const favoriteLibrarian = new UL.UniversityLibrarian();
// console.log(favoriteLibrarian)
// favoriteLibrarian.name = 'Anna'
// favoriteLibrarian.assistCustomer('Boris', 'LearnType')

// ------------------------------------Task08.06--------------------------
// const favoriteLibrarian = new UL.UniversityLibrarian();
// favoriteLibrarian.name = 'Anna'
// console.log(favoriteLibrarian.name)
// favoriteLibrarian.assistCustomer('Boris', 'LearnType')
// console.log(favoriteLibrarian)

// ----------------------------Task 08.07------------------------------------
// const refBook: RefBook = new RefBook(1, 'Learn TypeScript', 2022, 2)
// refBook.copies = 10;
// console.log(refBook.copies)

// ----------------------Task 09.01----------------------------
// console.log('Begin')
// getBooksByCategory(Category.JavaScript, logCategorySearch)
// getBooksByCategory(Category.Software, logCategorySearch)
// console.log('End')

// -------------------------------Task 09.02-------------------------------
// console.log('Begin')
// getBooksByCategoryPromise(Category.JavaScript)
//     .then(titles => {
//         console.log(titles);
//         return Promise.resolve(titles.length);
//     })
//     .then(n => console.log(n))
//     .catch(reason => console.log(reason))
// getBooksByCategoryPromise(Category.Software)
//     .then(titles => console.log(titles))
//     .catch(reason => console.log(reason))
// console.log('End')

// -------------------------Task 09.03---------------------
// console.log('Begin')
// logSearchResults(Category.JavaScript)
// logSearchResults(Category.Software).catch(err => console.log(err))
// console.log('End')